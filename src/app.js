const { config } = require('./config/config')

return config.isDev
	? console.log(process.env.DEV.split(' '))
	: console.log(process.env.PROD.split(' '))
