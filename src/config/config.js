const config = {}

config.isDev = process.env.NODE_ENV === 'development'

module.exports = { config }
